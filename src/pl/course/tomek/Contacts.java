package pl.course.tomek;


public class Contacts {
    private String name;
    private int number;

    public Contacts(String name, int number) {
        this.name = name;
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }
}
