package pl.course.tomek;

import java.util.Scanner;

public class Main {
    private static Scanner scanner = new Scanner(System.in);
    private static MobilePhone mobilePhone = new MobilePhone();

    public static void main(String[] args) {
        boolean quit = false;
        int choice = 0;
        printInstructions();
        while (!quit) {
            System.out.println("\nEnter number between 0 and 6 (enter 0 for instructions) :");
            choice = scanner.nextInt();
            scanner.nextLine();
            switch (choice) {
                case 0:
                    printInstructions();
                    break;
                case 1:
                    storeContact();
                    break;
                case 2:
                    modifyContact();
                    break;
                case 3:
                    removeContact();
                    break;
                case 4:
                    queryContact();
                    break;
                case 5:
                    printContacts();
                    break;
                case 6:
                    quit = true;
                    break;
            }
        }
    }

    private static void printInstructions() {
        System.out.println("\n Press:");
        System.out.println("\t 0 - to print instructions");
        System.out.println("\t 1 - to store contact");
        System.out.println("\t 2 - to modify contact");
        System.out.println("\t 3 - to remove contact");
        System.out.println("\t 4 - to find contact");
        System.out.println("\t 5 - for a list of contacts");
        System.out.println("\t 6 - to quit");
    }

    private static void printContacts() {
        mobilePhone.printList();
    }

    private static void queryContact() {
        System.out.println("Enter contact name to find:");
        mobilePhone.query(scanner.nextLine());
    }

    private static void removeContact() {
        System.out.println("Enter contact name to remove:");
        mobilePhone.remove(scanner.nextLine());
    }


    private static void modifyContact() {
        System.out.println("Enter contact name to modify:");
        String name = scanner.nextLine();
        if (mobilePhone.onFile(name)) {
            System.out.println("Enter new name:");
            String newName = scanner.nextLine();
            if (!mobilePhone.onFile(newName)) {
                mobilePhone.modify(name, newName);
            }
            else {
                System.out.println("Contact with this name already exists");
            }

        } else {
            System.out.println("Unable to find contact with specified name");
        }

    }

    private static void storeContact() {
        System.out.println("Enter new contact name:");
        String name = scanner.nextLine();
        if (!mobilePhone.onFile(name)) {
            System.out.println("Enter phone number:");
            int number = scanner.nextInt();
            scanner.nextLine();
            mobilePhone.store(name, number);
        } else {
            System.out.println("Contact with this name already exists");
        }
    }


}
