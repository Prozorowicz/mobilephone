package pl.course.tomek;

import java.util.ArrayList;

public class MobilePhone {
    private ArrayList<Contacts> contactsArrayList = new ArrayList<Contacts>();

    public void store(String name, int number) {
            Contacts contact = new Contacts(name, number);
            contactsArrayList.add(contact);
    }

    public void modify(String name, String newName) {

        int index = findContact(name);
        contactsArrayList.get(index).setName(newName);
    }

    public void remove(String name) {
        int index = findContact(name);
        if (index >= 0) {
            contactsArrayList.remove(index);
            System.out.println(name+", was removed");
        } else {
            System.out.println("Unable to find contact with specified name");
        }
    }

    public void query(String name) {
        int index = findContact(name);
        if (index >= 0) {
            Contacts contact = contactsArrayList.get(index);
            System.out.println("name:   " + contact.getName() + "\n" + "number: " + contact.getNumber());
        } else {
            System.out.println("Unable to find contact with specified name");
        }
    }

    public void printList() {
        for (int i = 0; i < contactsArrayList.size(); i++) {
            Contacts contact = contactsArrayList.get(i);
            System.out.println("name:   " + contact.getName() + "\n" + "number: " + contact.getNumber() + "\n");
        }
    }

    private int findContact(String name) {
        for (int i = 0; i < contactsArrayList.size(); i++) {
            if (contactsArrayList.get(i).getName().equals(name)) {
                return i;
            }
        }
        return -1;
    }

    public boolean onFile(String name) {
        if (findContact(name) >= 0) {
            return true;
        } else return false;

    }

}
